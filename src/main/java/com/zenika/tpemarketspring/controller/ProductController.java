package com.zenika.tpemarketspring.controller;

import cli.Store;
import domain.ProductLine;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class ProductController {

    @GetMapping("store")
    public List<ProductLine> getStore() {
        return Store.getInstance().getStore().getProducts();
    }
}
