package com.zenika.tpemarketspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpEmarketSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(TpEmarketSpringApplication.class, args);
    }

}
