package domain;

import exception.BasketException;

import java.util.ArrayList;
import java.util.List;

public class Basket {

    private List<ProductLine> products = new ArrayList<>();

    public List<ProductLine> getProducts() {
        return products;
    }

    public ProductLine getProductLineById(int id) {
        for (ProductLine productLine : products) {
            if (productLine.getProduct().getId() == id) {
                return productLine;
            }
        }
        throw new BasketException("Le produit n'est pas présent dans votre panier !");
    }

    public Product getProductById(int id) {
        for (ProductLine productLine : products) {
            if (productLine.getProduct().getId() == id) {
                return productLine.getProduct();
            }
        }
        throw new BasketException("Le produit n'est pas présent dans votre panier !");
    }

    public boolean isProductPresent(Product product) {
        for (ProductLine productLine : products) {
            if (productLine.getProduct().equals(product)) {
                return true;
            }
        }
        return false;
    }

    public void addProduct(Product product, int quantity) {
        if (quantity <= 0) {
            throw new BasketException("Quantité incorrect !");
        }
        if (isProductPresent(product)) {
            ProductLine productLine = getProductLineById(product.getId());
            productLine.setQuantity(productLine.getQuantity() + quantity);
        } else {
            products.add(new ProductLine(product, quantity));
            if (product.getId() == 0) {
                product.setId(products.size());
            }
        }
    }

    public void removeProduct(Product product, int quantity) {
        if (quantity < 0) {
            throw new BasketException("Veuillez entrer une quantité positive !");
        }
        int actualQuantity = getProductLineById(product.getId()).getQuantity();
        if (actualQuantity < quantity) {
            throw new BasketException(("La quantité du produit est supérieur à celle souhaité !"));
        } else if (actualQuantity == quantity) {
            products.remove(getProductLineById(product.getId()));
        } else {
            getProductLineById(product.getId()).setQuantity(actualQuantity - quantity);
        }
    }

    public int getTotalPrice() {
        return products.stream().mapToInt(x -> x.getQuantity() * x.getProduct().getPrice()).sum();
    }

    public String toString() {
        StringBuilder basketContent = new StringBuilder();
        products.forEach((value) ->
                basketContent
                        .append("Id : ").append(value.getProduct().getId())
                        .append(" Nom : ").append(value.getProduct().getName())
                        .append(" Prix : ").append(value.getProduct().getPrice())
                        .append(" € Quantité : ").append(value.getQuantity()).append("\n"));

        return basketContent.toString();
    }

}
