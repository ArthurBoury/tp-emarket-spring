package domain;

import java.util.List;

public class Order {
    private final int totalPrice;
    private final List<ProductLine> products;
    private final Customer customer;

    public Order(List<ProductLine> products, Customer customer) {
        this.products = products;
        this.customer = customer;
        this.totalPrice = products.stream().mapToInt(x -> x.getQuantity() * x.getProduct().getPrice()).sum();
    }

    public List<ProductLine> getProducts() {
        return products;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public String toString() {
        StringBuilder orderContent = new StringBuilder();

        orderContent.append("Récapitulatif de la commande : \n").
                append(customer.toString()).append("\n");
        products.forEach((value) ->
                orderContent.
                        append(value).append(" x ").append(value.getProduct().getName()).append(" ").
                        append(value.getQuantity() * value.getProduct().getPrice()).append(" €\n"));
        orderContent.append("Prix total : ").append(totalPrice).append(" €");

        return orderContent.toString();
    }
}
