package domain;

import exception.ProductException;

import java.util.Objects;

public class Product {
    private int id;
    private String name;
    private int price;

    public Product(String name, int price) {
        if (name.equals("")) {
            throw new ProductException("Le nom du produit ne peut être vide !");
        }
        if (price < 0) {
            throw new ProductException("Le prix ne peut être inférieur à 0");
        }
        this.name = name;
        this.price = price;
        this.id = 0;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id && price == product.price && Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price);
    }
}
