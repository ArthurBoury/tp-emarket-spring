package domain;

public class Customer {
    String address;

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAdress() {
        return address;
    }

    public String toString(){
        return "Adresse : " + this.address;
    }
}
