package file;

import domain.Basket;
import domain.Product;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class ReadWriteFile {
    public static Basket readFile(String fileName) {
        String inputFile = "C:\\Users\\Arthur Boury\\IdeaProjects\\tp-emarket\\src\\main\\resources\\" + fileName;
        Basket basket = new Basket();
        try {
            List<String> lines = Files.readAllLines(Path.of(inputFile).toAbsolutePath());
            for (String line : lines.subList(1, lines.size())) {
                String[] products = line.split(",");
                Product product = new Product(products[0], Integer.parseInt(products[1]));
                basket.addProduct(product, Integer.parseInt(products[2]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return basket;
    }

    public static void printOrderFile(String orderSummary, String file) {
        String outPutFile = "src/main/resources/ordersCustomer" + file;
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(outPutFile);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(orderSummary);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
