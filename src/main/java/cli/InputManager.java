package cli;

import java.util.Scanner;

public class InputManager {
    private static final Scanner scanner = new Scanner(System.in);
    private static final InputManager INSTANCE = new InputManager();

    public static InputManager getInstance() {
        return INSTANCE;
    }

    private InputManager() {
    }

    public String getCustomerAddressEntry() {
        return scanner.nextLine();
    }

    public int getCustomerChoice() {
        Integer choice = null;
        do {
            try {
                choice = Integer.parseInt(scanner.nextLine());
            } catch (Exception e) {
                System.out.println("Choix invalide !");
            }
        } while (choice == null);

        return choice;
    }
}
