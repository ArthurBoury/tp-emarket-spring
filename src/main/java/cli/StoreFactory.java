package cli;

import domain.Basket;
import file.ReadWriteFile;

public class StoreFactory {
    public static Basket createStore(String fileName) {
        return ReadWriteFile.readFile(fileName);
    }
}
