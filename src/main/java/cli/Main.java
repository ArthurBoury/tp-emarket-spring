package cli;

import cli.screen.HelloScreen;
import cli.screen.Screen;

public class Main {

    public static void main(String[] args) {
        Screen.launchWorkflow(new HelloScreen());
    }
}
