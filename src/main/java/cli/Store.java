package cli;

import domain.Basket;

public class Store {
    public Basket getStore() {
        return store;
    }

    private final Basket store = StoreFactory.createStore("data1.csv");
    private static final Store INSTANCE = new Store();


    public static Store getInstance() {
        return INSTANCE;
    }
}
