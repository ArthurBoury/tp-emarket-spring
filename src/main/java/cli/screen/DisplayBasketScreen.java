package cli.screen;

import cli.CustomerManager;
import domain.Basket;

public class DisplayBasketScreen implements Screen {
    @Override
    public Screen render() {
        Basket basket = CustomerManager.getInstance().getBasket();
        System.out.println(basket.toString());
        return new MainMenuScreen();
    }
}
