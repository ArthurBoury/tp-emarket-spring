package cli.screen;

import cli.InputManager;

public class MainMenuScreen implements Screen {
    @Override
    public Screen render() {
        System.out.println("""
                1. Ajouter un produit au panier
                2. Supprimer un produit du panier
                3. Consulter le panier
                4. Valider son panier
                5. Consulter ses commandes
                6. Quitter l'application
                """);
        int choice = InputManager.getInstance().getCustomerChoice();
        return switch (choice) {
            case 1 -> new AddProductScreen();
            case 2 -> new RemoveProductScreen();
            case 3 -> new DisplayBasketScreen();
            case 4 -> new BasketValidationScreen();
            case 5 -> new DisplayOrdersScreen();
            case 6 -> new ByeByeScreen();
            default -> this;
        };
    }
}
