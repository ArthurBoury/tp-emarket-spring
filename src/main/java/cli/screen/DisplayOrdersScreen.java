package cli.screen;

import cli.CustomerManager;
import domain.Order;

public class DisplayOrdersScreen implements Screen {
    @Override
    public Screen render() {
        for (Order order : CustomerManager.getInstance().getOrders()) {
            System.out.println(order.toString());
            System.out.println("******************************");
        }
        return new MainMenuScreen();
    }
}
