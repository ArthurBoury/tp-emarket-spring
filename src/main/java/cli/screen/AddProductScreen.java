package cli.screen;

import cli.CustomerManager;
import cli.InputManager;
import cli.Store;
import domain.Basket;
import domain.Product;
import exception.BasketException;

import java.util.NoSuchElementException;

public class AddProductScreen implements Screen {

    @Override
    public Screen render() {
        Basket store = Store.getInstance().getStore();
        System.out.println(store.toString());

        System.out.println("Quelle produit souhaitez vous ajouter ? ");
        int choice = InputManager.getInstance().getCustomerChoice();

        try {
            Product product = store.getProductById(choice);

            System.out.println("Combien de " + product.getName().toLowerCase() + " souhaitez vous acheter ?");

            int quantity = InputManager.getInstance().getCustomerChoice();
            CustomerManager.getInstance().getBasket().addProduct(product, quantity);
            store.removeProduct(product, quantity);

        } catch (NoSuchElementException | BasketException e) {
            System.out.println(e.getMessage());
            return this;
        }
        return new MainMenuScreen();
    }
}

