package cli.screen;

import cli.CustomerManager;
import cli.InputManager;
import domain.Basket;
import domain.Customer;
import domain.Order;
import file.ReadWriteFile;

import java.util.List;

public class FinalizeOrderScreen implements Screen {
    @Override
    public Screen render(){
        Basket basket = CustomerManager.getInstance().getBasket();
        Customer customer = CustomerManager.getInstance().getCustomer();
        List<Order> orders = CustomerManager.getInstance().getOrders();

        System.out.println("Veuillez entrer votre adresse : ");
        String address = InputManager.getInstance().getCustomerAddressEntry();
        customer.setAddress(address);
        Order order = new Order(basket.getProducts(), customer);
        orders.add(order);
        System.out.println(order.toString());

        ReadWriteFile.printOrderFile(order.toString(), String.valueOf(basket.hashCode()));

        CustomerManager.getInstance().setBasket(new Basket());
        return new MainMenuScreen();
    }
}
