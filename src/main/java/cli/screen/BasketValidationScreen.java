package cli.screen;

import cli.InputManager;

public class BasketValidationScreen implements Screen {
    @Override
    public Screen render() {
        System.out.println("""
                Souhaitez vous vraiment finaliser vos achats ?
                1) Oui
                2) Non
                """);

        int choice = InputManager.getInstance().getCustomerChoice();
        return switch (choice) {
            case 1 -> new FinalizeOrderScreen();
            case 2 -> new MainMenuScreen();
            default -> this;
        };
    }
}
