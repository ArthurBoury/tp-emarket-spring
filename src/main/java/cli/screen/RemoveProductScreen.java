package cli.screen;

import cli.CustomerManager;
import cli.InputManager;
import cli.Store;
import domain.Product;
import exception.BasketException;

import java.util.NoSuchElementException;

public class RemoveProductScreen implements Screen {
    @Override
    public Screen render() {
        System.out.println(CustomerManager.getInstance().getBasket().toString());

        System.out.println("Quel produit voulez vous supprimer ? ");
        int productChoice = InputManager.getInstance().getCustomerChoice();
        try {
            Product product = CustomerManager.getInstance().getBasket().getProductById(productChoice);
            System.out.println("Combien de " + product.getName().toLowerCase() + " souhaitez vous supprimer ?");

            int quantity = InputManager.getInstance().getCustomerChoice();

            CustomerManager.getInstance().getBasket().removeProduct(product, quantity);
            Store.getInstance().getStore().addProduct(product, quantity);
        } catch (NoSuchElementException | BasketException e) {
            System.out.println(e.getMessage());
            return this;
        }
        return new MainMenuScreen();
    }
}
