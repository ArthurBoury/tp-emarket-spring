package cli;

import domain.Basket;
import domain.Customer;
import domain.Order;

import java.util.ArrayList;
import java.util.List;

public class CustomerManager {
    private Basket basket = new Basket();
    private static final Customer customer = new Customer();
    private static final List<Order> orders = new ArrayList<>();

    private static final CustomerManager INSTANCE = new CustomerManager();

    private CustomerManager() {
    }

    public static CustomerManager getInstance() {
        return INSTANCE;
    }

    public Basket getBasket() {
        return basket;
    }

    public Customer getCustomer() {
        return customer;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }
}
