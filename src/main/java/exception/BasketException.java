package exception;

public class BasketException extends RuntimeException {
    public BasketException() {
        super();
    }

    public BasketException(String message) {
        super(message);
    }
}
